﻿using System;
using Foundation;
using ObjCRuntime;
using UIKit;

namespace SZCAAuth
{
    // @interface SZCAAuthInfo : NSObject
    [BaseType(typeof(NSObject))]
    interface SZCAAuthInfo
    {
        // @property (nonatomic, strong) NSString * bankCard;
        [Export("bankCard", ArgumentSemantic.Strong)]
        string BankCard { get; set; }

        // @property (nonatomic, strong) NSString * phoneNumber;
        [Export("phoneNumber", ArgumentSemantic.Strong)]
        string PhoneNumber { get; set; }

        // @property (nonatomic, strong) UIImage * handWrittenImg;
        [Export("handWrittenImg", ArgumentSemantic.Strong)]
        UIImage HandWrittenImg { get; set; }

        // @property (nonatomic, strong) SZCAIDCardInfo * idCardInfo;
        [Export("idCardInfo", ArgumentSemantic.Strong)]
        SZCAIDCardInfo IdCardInfo { get; set; }

        // @property (nonatomic, strong) NSString * token;
        [Export("token", ArgumentSemantic.Strong)]
        string Token { get; set; }

        // @property (nonatomic, strong) NSString * certDn;
        [Export("certDn", ArgumentSemantic.Strong)]
        string CertDn { get; set; }

        // @property (nonatomic, strong) NSString * certSn;
        [Export("certSn", ArgumentSemantic.Strong)]
        string CertSn { get; set; }
    }

    // @interface SZCAAuthSDK : NSObject
    [BaseType(typeof(NSObject))]
    interface SZCAAuthSDK
    {
        // +(instancetype)SZCAAuthShared;
        [Static]
        [Export("SZCAAuthShared")]
        SZCAAuthSDK SZCAAuthShared();

        // -(int)verifySDKLicense:(NSString *)license;
        [Export("verifySDKLicense:")]
        int VerifySDKLicense(string license);

        // -(SZCA_AUTH_INIT_ENUM)initialize:(NSString *)account keyTpye:(NSString *)keytype pwd:(NSString *)pwd;
        [Export("initialize:keyTpye:pwd:")]
        SzcaAuthInitEnum Initialize(string account, string keytype, string pwd);

        // -(void)finalize;
        [Export("finalize")]
        void Finalize();

        // -(void)setBaseURL:(NSString *)baseURL;
        [Export("setBaseURL:")]
        void SetBaseURL(string baseURL);

        // -(BOOL)isSDKValid:(NSError **)error;
        [Export("isSDKValid:")]
        bool IsSDKValid(out NSError error);

        // -(void)authIdentityWithController:(UIViewController *)viewController completion:(void (^)(SZCAAuthInfo *, NSError *))completion;
        [Export("authIdentityWithController:completion:")]
        void AuthIdentityWithController(UIViewController viewController, Action<SZCAAuthInfo, NSError> completion);

        // -(void)reissueCertWithController:(UIViewController *)viewController completion:(void (^)(SZCAAuthInfo *, NSError *))completion;
        [Export("reissueCertWithController:completion:")]
        void ReissueCertWithController(UIViewController viewController, Action<SZCAAuthInfo, NSError> completion);

        // -(void)resetPinWithController:(UIViewController *)viewController account:(NSString *)account completion:(void (^)(NSError *))completion;
        [Export("resetPinWithController:account:completion:")]
        void ResetPinWithController(UIViewController viewController, string account, Action<NSError> completion);

        // -(void)signFileWithFileId:(NSString *)fileId hash:(NSString *)hash hasTsa:(NSString *)hasTsa completion:(void (^)(NSData *))completion failure:(void (^)(NSError *))failure;
        [Export("signFileWithFileId:hash:hasTsa:completion:failure:")]
        void SignFileWithFileId(string fileId, string hash, string hasTsa, Action<NSData> completion, Action<NSError> failure);

        // -(NSString *)getCertSn;
        [Export("getCertSn")]
        string CertSn { get; }

        // -(NSString *)getCertDn;
        [Export("getCertDn")]
        string CertDn { get; }

        // -(int)setCertSN:(NSString *)serialNum;
        [Export("setCertSN:")]
        int SetCertSN(string serialNum);

        // -(int)setCertDN:(NSString *)certDn;
        [Export("setCertDN:")]
        int SetCertDN(string certDn);

        // -(NSString *)createP10WithSuject:(NSString *)suject;
        [Export("createP10WithSuject:")]
        string CreateP10WithSuject(string suject);

        // -(void)requestCert:(SZCAReqApplyModel *)model completion:(void (^)(NSString *, NSString *))completionBlock failure:(void (^)(NSError *))failureBlock;
        [Export("requestCert:completion:failure:")]
        void RequestCert(SZCAReqApplyModel model, Action<NSString, NSString> completionBlock, Action<NSError> failureBlock);
    }

    // @interface SZCAIDCardInfo : NSObject
    [BaseType(typeof(NSObject))]
    interface SZCAIDCardInfo
    {
        // @property (copy, nonatomic) NSString * name;
        [Export("name")]
        string Name { get; set; }

        // @property (copy, nonatomic) NSString * idNo;
        [Export("idNo")]
        string IdNo { get; set; }

        // @property (copy, nonatomic) NSString * sex;
        [Export("sex")]
        string Sex { get; set; }

        // @property (copy, nonatomic) NSString * race;
        [Export("race")]
        string Race { get; set; }

        // @property (copy, nonatomic) NSString * birthDay;
        [Export("birthDay")]
        string BirthDay { get; set; }

        // @property (copy, nonatomic) NSString * address;
        [Export("address")]
        string Address { get; set; }

        // @property (nonatomic, strong) NSString * imgCardFront;
        [Export("imgCardFront", ArgumentSemantic.Strong)]
        string ImgCardFront { get; set; }

        // @property (nonatomic, strong) NSString * imgCardBack;
        [Export("imgCardBack", ArgumentSemantic.Strong)]
        string ImgCardBack { get; set; }

        // @property (copy, nonatomic) NSString * issue;
        [Export("issue")]
        string Issue { get; set; }

        // @property (copy, nonatomic) NSString * period;
        [Export("period")]
        string Period { get; set; }
    }

    // @interface SZCAReqApplyModel : NSObject
    [BaseType(typeof(NSObject))]
    interface SZCAReqApplyModel
    {
        // @property (nonatomic, strong) NSString * userName;
        [Export("userName", ArgumentSemantic.Strong)]
        string UserName { get; set; }

        // @property (nonatomic, strong) NSString * idNo;
        [Export("idNo", ArgumentSemantic.Strong)]
        string IdNo { get; set; }

        // @property (nonatomic, strong) NSString * token;
        [Export("token", ArgumentSemantic.Strong)]
        string Token { get; set; }

        // @property (nonatomic, strong) NSString * sex;
        [Export("sex", ArgumentSemantic.Strong)]
        string Sex { get; set; }

        // @property (nonatomic, strong) NSString * mobileNo;
        [Export("mobileNo", ArgumentSemantic.Strong)]
        string MobileNo { get; set; }

        // @property (nonatomic, strong) NSString * identityImgOne;
        [Export("identityImgOne", ArgumentSemantic.Strong)]
        string IdentityImgOne { get; set; }

        // @property (nonatomic, strong) NSString * identityImgTwo;
        [Export("identityImgTwo", ArgumentSemantic.Strong)]
        string IdentityImgTwo { get; set; }

        // @property (nonatomic, strong) NSString * signImg;
        [Export("signImg", ArgumentSemantic.Strong)]
        string SignImg { get; set; }

        // @property (nonatomic, strong) NSString * humanBodyImg;
        [Export("humanBodyImg", ArgumentSemantic.Strong)]
        string HumanBodyImg { get; set; }

        // @property (nonatomic, strong) NSString * province;
        [Export("province", ArgumentSemantic.Strong)]
        string Province { get; set; }

        // @property (nonatomic, strong) NSString * city;
        [Export("city", ArgumentSemantic.Strong)]
        string City { get; set; }

        // @property (nonatomic, strong) NSString * contactAddr;
        [Export("contactAddr", ArgumentSemantic.Strong)]
        string ContactAddr { get; set; }

        // @property (nonatomic, strong) NSString * cardedPlace;
        [Export("cardedPlace", ArgumentSemantic.Strong)]
        string CardedPlace { get; set; }

        // @property (nonatomic, strong) NSString * cardedExpiryDate;
        [Export("cardedExpiryDate", ArgumentSemantic.Strong)]
        string CardedExpiryDate { get; set; }

        // @property (nonatomic, strong) NSString * card;
        [Export("card", ArgumentSemantic.Strong)]
        string Card { get; set; }

        // @property (nonatomic, strong) NSString * carrier;
        [Export("carrier", ArgumentSemantic.Strong)]
        string Carrier { get; set; }

        // @property (nonatomic, strong) NSString * appType;
        [Export("appType", ArgumentSemantic.Strong)]
        string AppType { get; set; }
    }
}
