﻿
namespace SZCAAuth
{
    public enum SzcaAuthInitEnum : uint
    {
        SuccesHasSignKey,
        SuccesNoSignKey,
        FailurePinError,
        FailureError
    }
}
