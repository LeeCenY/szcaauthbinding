﻿using System;
using Foundation;
using SZCAAuth;
using UIKit;

namespace SZCAAuth_Xamarin
{
    public partial class ViewController : UIViewController
    {
        protected ViewController(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            var authSDK = SZCAAuthSDK.SZCAAuthShared();
            var valid = authSDK.Initialize("test001", "RSA", "123456");
            Console.WriteLine(valid);


        }

    }
}
